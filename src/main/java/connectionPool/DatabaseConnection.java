package connectionPool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private String userName = "postgres";
    private String password = "postgres";
    private String url = "jdbc:postgresql://localhost:5432/clientserver";
    private Connection connection;
    private boolean avaible;

    public DatabaseConnection() {
        this.connection = makeConnection();
        this.avaible = true;
    }

    public Connection getConnection() {
        return connection;
    }

    private Connection makeConnection() {
        try {
            return DriverManager.getConnection(url, userName, password);
        } catch (SQLException e) {
            System.out.println("Cannot connect to the database");
        }
        return null;
    }

    public void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void setIsAvaible(boolean isAvaible) {
        this.avaible = isAvaible;
    }

    public boolean isAvaible() {
        return avaible;
    }
}

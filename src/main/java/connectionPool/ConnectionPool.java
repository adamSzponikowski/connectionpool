package connectionPool;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private final int MAXIMUM_CONNECTIONS = 100;
    private volatile List<DatabaseConnection> connectionPool = initialList();
    private ReentrantLock lock = new ReentrantLock();

    private DatabaseConnection databaseConnection() {
        return new DatabaseConnection();
    }

    public void dropConnection(DatabaseConnection databaseConnection) {
        if (connectionPool.size() > MAXIMUM_CONNECTIONS) {
            connectionPool.remove(databaseConnection);
            databaseConnection.closeConnection();
        } else {
            databaseConnection.setIsAvaible(true);
        }
    }

    public DatabaseConnection getOrCreateConnection() throws SQLException {
        lock.lock();
        try {
            DatabaseConnection databaseConnection = null;
            if (isConnectionAvaible()) {
                databaseConnection = getFirstFreeConnection();
            } else {
                if (connectionPool.size() < MAXIMUM_CONNECTIONS) {
                    databaseConnection = databaseConnection();
                    connectionPool.add(databaseConnection);
                    databaseConnection = getFirstFreeConnection();
                } else {
                    System.out.println("YOU CAN NOT ADD MORE CONNECTIONS");
                    removeNotUsedConnections();
                    getOrCreateConnection();
                }
            }
            if (databaseConnection != null) {
                databaseConnection.setIsAvaible(false);
            }
            return databaseConnection;
        } finally {
            lock.unlock();
        }
    }


    private DatabaseConnection getFirstFreeConnection() throws SQLException {
        DatabaseConnection databaseConnection = null;
        while (databaseConnection == null) {
            databaseConnection = connectionPool
                    .stream()
                    .filter(DatabaseConnection::isAvaible)
                    .findFirst()
                    .orElseThrow(() -> new NoSuchElementException("No free connections available"));
            databaseConnection = (databaseConnection.getConnection().isValid(1)) ? databaseConnection : null;
        }
        return databaseConnection;
    }


    private void removeNotUsedConnections() {
        if (connectionPool.size() >= MAXIMUM_CONNECTIONS) {
            connectionPool.stream()
                    .filter(DatabaseConnection::isAvaible)
                    .findFirst()
                    .ifPresent(databaseConnection -> {
                        connectionPool.remove(databaseConnection);
                        databaseConnection.closeConnection();
                    });
        }
    }

    private List<DatabaseConnection> initialList() {
        LinkedList<DatabaseConnection> initialList = new LinkedList<>();
        for (int x = 0; x < 5; x++) {
            initialList.add(new DatabaseConnection());
        }
        return initialList;
    }

    private boolean isConnectionAvaible() {
        return connectionPool.stream().anyMatch(DatabaseConnection::isAvaible);
    }


}
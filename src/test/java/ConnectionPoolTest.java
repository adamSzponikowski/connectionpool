import connectionPool.ConnectionPool;
import connectionPool.DatabaseConnection;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConnectionPoolTest {
    private ConnectionPool connectionPool = new ConnectionPool();

    @Test
    public void connectionPoolStressTest() throws SQLException, InterruptedException {
        //Given
        int expected_rows = 10000;
        CountDownLatch latch = new CountDownLatch(expected_rows);
        ExecutorService executorService = new ThreadPoolExecutor(110, 110,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
//When
        for (int x = 0; x < expected_rows; x++) {
            executorService.submit(() -> {
                DatabaseConnection databaseConnection = null;
                try {
                    databaseConnection = connectionPool.getOrCreateConnection();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                if (databaseConnection != null) {
                    Connection connection = databaseConnection.getConnection();
                    try {
                        connection.createStatement().execute("INSERT INTO example_table (id) VALUES (1)");
                        connectionPool.dropConnection(databaseConnection);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                    latch.countDown();
                }
            });
        }
        latch.await();
        //Then
        DatabaseConnection databaseConnection = connectionPool.getOrCreateConnection();
        ResultSet resultSet = databaseConnection.getConnection()
                .createStatement()
                .executeQuery("SELECT COUNT(1) FROM example_table");
        resultSet.next();
        int counter = resultSet.getInt(1);
        assertEquals(expected_rows, counter);
    }

}
